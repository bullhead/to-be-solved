package com.bullhead.rx.problem;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Edition implements Serializable {
    @SerializedName("EditionId")
    private long    id;
    @SerializedName("Name")
    private String  name;
    @SerializedName("Region")
    private String  region;
    @SerializedName("Country")
    private String  country;
    @SerializedName("Status")
    private boolean status;
    private int     sequence;
}
