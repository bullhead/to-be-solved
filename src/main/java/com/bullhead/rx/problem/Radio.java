package com.bullhead.rx.problem;


import lombok.*;

import java.io.Serializable;
import java.util.Objects;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Radio implements Serializable {
    private long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Radio radio = (Radio) o;
        return id == radio.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
