package com.bullhead.rx.problem;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface AppService {
    @GET("radios")
    Single<GetRadioResponse> getRadios(@Query("id") long id);


    @GET("editions")
    Observable<List<Edition>> getEditions();
}
