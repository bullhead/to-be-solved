package com.bullhead.rx.problem;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Problem {
    public static void main(String... args) {
        AppService appService = new Retrofit.Builder()
                .baseUrl("http://iamlost.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(AppService.class);

        //here you go
    }
}
