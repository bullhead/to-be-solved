package com.bullhead.rx.problem;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetRadioResponse implements Serializable {
    @SerializedName("Authors")
    private List<Radio> data;
}
